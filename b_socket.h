/*  b_socket.h - Prototypes of the sockets in the IRC bot.
 *  ======================================================
 *
 *  This file contains the prototypes of the function(s)
 *  that will be used by the bot to stablish a connection
 *  between the IRC server and the IRC bot.
 *  ======================================================
 *  Copyright (C) 2009  "Carlos Ríos Vera" <crosvera@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 ***********************************************************/

#ifndef _B_SOCKET_H
#define _B_SOCKET_H

#include "config.h"

/* Other macros */
//#define printlog(MSG)   printf("$BOT > %s", MSG)

/* The `set_socket' function will (as the name says) set the
 * socket which allow the comunication between the IRC server
 * and the bot. This function need a pointer to a
 * `struct _config' structure which contains the information
 * of the connection (such as the server, nick, and so on).
 * This function return an `int' value, this value is defined
 * in the macros (IRC_RECONNECT, ERROR_EXIT, SUCCESS_EXIT)
 * before in this file.
 ***********************************************************/
int set_socket( struct _config *config );

#endif /* _B_SOCKET_H */
