/*  log.h - Prototypes of Cbot-log functions
 * ========================================================
 *
 * This file contains the prototypes of the functions which
 * reads the config file. This config file will contains 
 * the IRC server, port , channel where the bot will
 * connect, and the nickname of the bot.
 * ========================================================
 *  Copyright (C) 2009  "Carlos Ríos Vera" <crosvera@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 ************************************************************/
#ifndef _LOG_H
#define _LOG_H

#include <stdbool.h>

#include "config.h"

/* This global variable (boolean) defines if we show 
 * or not the log messages via stdout               */
bool SHOW_LOG;
/* This global variable (boolean) defines if we save
 * or not the log messages in 'config->log_file'    */
bool SAVE_LOG;


#define printlog(MSG)   log_msg( MSG, config )

/* The follow function will show and save the logs
 * only if SHOW_LOG & SAVE_LOG are setted as true   */ 
void log_msg( char *msg, struct _config *config );

#endif /* _LOG_H */
