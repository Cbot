/*  config.h - Prototypes of the configuration's functions
 * ========================================================
 *
 * This file contains the prototypes of the functions which
 * reads the config file. This config file will contains 
 * the IRC server, port , channel where the bot will
 * connect, and the nickname of the bot.
 * ========================================================
 *  Copyright (C) 2009  "Carlos Ríos Vera" <crosvera@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 ************************************************************/
#ifndef     _CONFIG_H
#define     _CONFIG_H

/*** Some macros definitions ***/
#define IRC_RECONNECT    1
#define ERROR_EXIT      -1
#define SUCCESS_EXIT     0

//#define     CONFIG_FILE     "./cbot.conf"
char    *CONFIG_FILE;

/* The follow structure sets the information that contains
 * the config file.
 ************************************************************/
struct _config{
    /* IRC Config */
    char irc_server[129];
    char irc_port[6];
    char irc_channel[16];
    char irc_nick[16];
    char irc_name[30];
    char username[30];
    char master[16];

    /* Log Config */
    char log_file[256];

};

/* The `set_config' function will read CONFIG_FILE and will
 * set the information required to the irc-bot. The required
 * information will be stored into a `_config' structure.
 ***********************************************************/
struct _config  *set_config( void );

#endif /* _CONFIG_H */
