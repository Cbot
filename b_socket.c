/*  b_socket.c - cbot socket set up
 * ======================================================
 *  Copyright (C) 2009  "Carlos Ríos Vera" <crosvera@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 ********************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>

#include "b_socket.h"
#include "config.h"
#include "irc_cmd.h"
#include "log.h"

int set_socket( struct _config *config ){
    if( config == NULL ){
        printlog("***ERROR*** There was an error with the configuration file.");
        return ERROR_EXIT;
    }

    char buffer[1025];
    struct addrinfo hints, *res;
    int remote_socket;

    memset( &hints, 0, sizeof hints );
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;

    /* Get the `config->irc_server' adress */
    getaddrinfo( config->irc_server, config->irc_port, &hints, &res );

    /* Make the socket */
    remote_socket = socket( res->ai_family, res->ai_socktype, res->ai_protocol);

    /* connect! */
    connect( remote_socket, res->ai_addr, res->ai_addrlen );


    /*** SET NICK ***/
    char NICK[25];
    sprintf( NICK, "NICK %s\n", config->irc_nick);
    send( remote_socket, NICK, strlen(NICK), 0 );

    sleep(1);

    /*** SET USER ***/
    char USER[256];
    sprintf( USER, "USER %s %s haha :%s\n", config->username, config->irc_server, config->irc_name );
    send( remote_socket, USER, strlen(USER), 0 );

    //sleep(1);

    /*** JOIN INTO THE `config->irc_channel' ***/
    char JOIN[128];
    sprintf( JOIN, "JOIN %s\n", config->irc_channel );
    send( remote_socket, JOIN, strlen(JOIN), 0 );

    struct irc_msg *IRCmsg;

    for( ;; ){

	    if( recv( remote_socket, buffer, 1024, 0 ) > 0 )
    	    printlog(buffer);//printf( "%s", buffer );

        IRCmsg = irc_parser( buffer, config );

        if( IRCmsg != NULL ){
            if(IRCmsg->type == EXIT_RESTART){
                send( remote_socket, IRCmsg->msg, strlen(IRCmsg->msg), 0);
                //printf("$BOT > %s", IRCmsg->msg_log);
                printlog( IRCmsg->msg_log );
                /* CLOSE, FREE & EXIT! */
                close( remote_socket );
                freeaddrinfo( res );
                free( config );
                free(IRCmsg);
                return IRC_RECONNECT;
            }

            if(IRCmsg->type == NORMAL_MSG){
                send( remote_socket, IRCmsg->msg, strlen(IRCmsg->msg), 0 );
                //printf("$BOT > %s", IRCmsg->msg_log);
                printlog( IRCmsg->msg_log );
            }

            if(IRCmsg->type == EXIT){
                if( IRCmsg->msg != NULL )
                    send( remote_socket, IRCmsg->msg, strlen(IRCmsg->msg), 0 );

                printlog(IRCmsg->msg_log);
                /* CLOSE, FREE & EXIT! */
                close( remote_socket );
                freeaddrinfo( res );
                free( config );
                free(IRCmsg);

                return SUCCESS_EXIT;
            }

            free(IRCmsg);
        }

        /* Clean the buffer */
        memset( buffer, 0, sizeof(buffer) );
    }


    /* CLOSE & FREE!! */
    close( remote_socket );
    freeaddrinfo( res );
    free( config );

    return SUCCESS_EXIT;
}
