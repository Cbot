/*  irc_cmd.c - Functions which allows parse and execute
 *              a set of commands into the IRC bot.
 *  =====================================================
 *
 *  Copyright (C) 2009  "Carlos Ríos Vera" <crosvera@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 **********************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "irc_cmd.h"
#include "config.h"
#include "log.h"

static struct irc_msg *master_command( const char *raw_msg, struct _config *config ){

    char *ptr = NULL, *msg_to_send = NULL;
    struct irc_msg *IRCmsg = NULL;

    /* Did `config->master'said something important like a command to the bot ?
     * ***REMEMBER*** these commands must be sent via a PRIVMSG to the bot from
     * the `config->master'.
     ***************************************************************************/
    char *PRIVMSG = (char *)calloc( 1 +
            strlen("PRIVMSG  ") + strlen(config->irc_nick), sizeof(char) );

    sprintf( PRIVMSG, "PRIVMSG %s ", config->irc_nick );
    int privmsg_len = strlen(PRIVMSG);

    if( strncmp(PRIVMSG, raw_msg, privmsg_len) == 0 ){
        /* What command did `config->master` sent ? */

        /** the !say command? **/
        ptr = strstr( &raw_msg[privmsg_len], ":!say " );
        if( ptr != NULL ){
            IRCmsg = (struct irc_msg *)malloc( sizeof(struct irc_msg) );

            msg_to_send = (char *)calloc( 1 +
                strlen(&ptr[6]) + strlen("PRIVMSG  :") + strlen(config->irc_channel) , sizeof(char) );
            sprintf(msg_to_send, "PRIVMSG %s :%s", config->irc_channel, &ptr[6]);

            IRCmsg->type = NORMAL_MSG;
            IRCmsg->msg = msg_to_send;
            IRCmsg->msg_log = msg_to_send;

            return IRCmsg;
        }/*** END !say COMMAND */

        /** The !quit command? **/
        ptr = strstr( &raw_msg[privmsg_len], ":!quit");
        if( ptr != NULL ){
            IRCmsg = (struct irc_msg *)malloc( sizeof(struct irc_msg) );

            IRCmsg->type    = EXIT;
            IRCmsg->msg     = "QUIT";
            IRCmsg->msg_log = "EXIT command recived from the master.... BYE!\n";

            return IRCmsg;
        }/*** END !quit COMMAND **/

        /** The !raw command? **/
        ptr = strstr( &raw_msg[privmsg_len], ":!raw");
        if( ptr != NULL ){
            IRCmsg = (struct irc_msg *)malloc( sizeof(struct irc_msg) );

            IRCmsg->type    = NORMAL_MSG;
            IRCmsg->msg     = &ptr[6];
            IRCmsg->msg_log = &ptr[6];

            return IRCmsg;
        }/** END !raw COMMAND **/

        /** The !fjoin command? **/
        ptr = strstr( &raw_msg[privmsg_len], ":!fjoin");
        if( ptr != NULL ){
            IRCmsg = (struct irc_msg *)malloc( sizeof(struct irc_msg) );
            
            msg_to_send = (char *)calloc( 1 + strlen("JOIN \n") + strlen(config->irc_channel), sizeof(char) );
            sprintf( msg_to_send, "JOIN %s\n", config->irc_channel);

            IRCmsg->type    = NORMAL_MSG;
            IRCmsg->msg     = msg_to_send;
            IRCmsg->msg_log = "Forcing join into the channel\n";

            return IRCmsg;
        }/** END !rjoin command **/

    }

    return (struct irc_msg *)NULL;
}

struct irc_msg *irc_parser( const char *raw_entry, struct _config *config ){
    struct irc_msg *IRCmsg = NULL;
    IRCmsg = (struct irc_msg *)malloc( sizeof(struct irc_msg) );

    char *msg_to_send = NULL, *ptr = NULL;


    if( *raw_entry == ':' ){
        /* Did the `config->master' said something? */
        if( strncmp(&raw_entry[1], config->master, strlen(config->master)) == 0 ){
            /* Now we search the possition after the first ' ' in `raw_entry'
             * because that is the location of the message.                  */
            ptr = strchr( raw_entry, ' ' );
            ptr++;

            return master_command( ptr, config );
        }

    }else{
        /* Search the location after the first ' ' in `raw_entry' */
            ptr = strchr( raw_entry, ' ' );
            ptr++;

        /***PING RESPONSE***/
        if( strncmp( raw_entry, "PING", 4 ) == 0 ){
            msg_to_send = (char *)calloc(4 + strlen(&raw_entry[5]), sizeof(char));
            sprintf( msg_to_send, "PONG %s", &raw_entry[5] );
            IRCmsg->type    = NORMAL_MSG;
            IRCmsg->msg     = msg_to_send;
            IRCmsg->msg_log = msg_to_send;

            return IRCmsg;
        }

        /***KILL RESPONSE***/
        char *kill_msg = NULL;
        kill_msg = (char *)calloc(7 + strlen(config->irc_nick), sizeof(char));
        sprintf( kill_msg, "KILL %s\n", config->irc_nick);
        if( strncmp(kill_msg, ptr, strlen(kill_msg) ) == 0 ){
            char *msg2log = "\n*** The bot was killed, time to re-connect!!***\n";
            IRCmsg->type    = EXIT_RESTART;
            IRCmsg->msg     = NULL;
            IRCmsg->msg_log = msg2log;

            return IRCmsg;
        }
   }

   return NULL;
}
