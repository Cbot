/*  config.c - Functions which reads a config file to set the bot
 * ===============================================================
 *
 * The `CONFIG_FILE' macro is set in `config.h', this macro
 * sets the path where the config file is.
 * ===============================================================
 *  Copyright (C) 2009  "Carlos Ríos Vera" <crosvera@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *******************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include "config.h"

struct _config *set_config( void ){
    FILE *c_file = NULL;
    struct _config *config = NULL;
    char fline[512];

    /* Now we open the `CONFIG_FILE'. If an error happend, show a 
     * message, and return NULL.
     ****************************************************************/
    c_file = fopen( CONFIG_FILE, "r" );
    if( c_file == (FILE *)NULL ){
        fprintf( stderr, "\n%s: %s\n", CONFIG_FILE, strerror( errno ) );
        return (struct _config *)NULL;
    }

    config = (struct _config *) malloc( sizeof(struct _config) );


    /* Now we read the `CONFIG_FILE' and store the info into
     * the config structure.
     ***************************************************************/
    while( fgets(fline, sizeof(fline), c_file) ){

        if( sscanf( fline, "server: %s\n", config->irc_server ) == 1 )
            continue;
        if( sscanf( fline, "port: %s\n", config->irc_port ) == 1 )
            continue;
        if( sscanf( fline, "channel: %s\n", config->irc_channel ) == 1 )
            continue;
        if( sscanf( fline, "nick: %s\n", config->irc_nick ) == 1)
            continue;
        if( sscanf( fline, "name: %[^\n]\n", config->irc_name ) == 1 )
            continue;
        if( sscanf( fline, "username: %[^\n]\n", config->username ) == 1 )
            continue;
        if( sscanf( fline, "master: %s\n", config->master ) == 1 )
            continue;
        if( sscanf( fline, "log-file: %[^\n]\n", config->log_file) == 1)
            continue;
    }
    fclose(c_file);

    if(   strlen(config->irc_server) > 1  &&  strlen(config->irc_port) > 1
       && strlen(config->irc_channel) > 1  &&  strlen(config->irc_nick) > 1
       && strlen(config->irc_name) > 1  && strlen(config->username) > 1
       && strlen(config->master) > 1  &&  strlen(config->log_file) > 1      )
    {/* If there was no problems, go ahead !! */
        return config;
    }

    /* troubles? */
    free(config);
    return (struct _config *)NULL;


}
