/*** Cbot.c - Cbot, the IRC bot written in C ***/
/*** Version 0.2 ***/
/*  Copyright (C) 2009  "Carlos Ríos Vera" <crosvera@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "log.h"
#include "config.h"
#include "b_socket.h"

void parse_opt( int argc, char *argv[] ){
    char *default_file = "./cbot.conf";
    if( argc == 1 ){
        SHOW_LOG = false;
        SAVE_LOG = false;
        CONFIG_FILE = default_file;

        return;
    }

    CONFIG_FILE = NULL;
    int x;
    for( x = 1; x < argc; x++ ){
        if( strncmp( "--save-log", argv[x], 10 ) == 0 ){
            SAVE_LOG = true;
            if( CONFIG_FILE == NULL )
                CONFIG_FILE = default_file;
            continue;
        }
        if( strncmp( "--verbose", argv[x], 9 ) == 0 ){
            SHOW_LOG = true;
            if( CONFIG_FILE == NULL )
                CONFIG_FILE = default_file;
            continue;
        }
        if( strncmp("--config-file", argv[x], 13 ) == 0){
            x += 1;
            if(x < argc)
                CONFIG_FILE = argv[x];
            continue;
        }
    }
}


int main( int argc, char **argv ){

    parse_opt( argc, argv );

    int result = IRC_RECONNECT;
    struct _config *config = NULL;
    config = set_config();

    while( result == IRC_RECONNECT ){
        result = set_socket( config );
    }

    switch( result ){
        case ERROR_EXIT: /* There was an error. we need to exit */
            printlog("***ERROR*** There was an error, for further information check the log.\n");
            break;

        case SUCCESS_EXIT: /* There was an order to exit and there are no problems arround */
            printlog("Exiting with success... =)\n");
            break;

        default: /* Something unknown happend */
            printlog("***ERROR*** Something (wrong) happend, please check the log and send us a bug-report!\n");
            
    }

    return 0;/* EXIT */

}
