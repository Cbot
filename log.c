/*  log.c - Functions which save the logs of Cbot
 * ===============================================================
 *
 * The `CONFIG_FILE' macro is set in `config.h', this macro
 * sets the path where the config file is.
 * ===============================================================
 *  Copyright (C) 2009  "Carlos Ríos Vera" <crosvera@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>


#include "log.h"
#include "config.h"


void log_msg( char *msg, struct _config *config ){
    char *log_file = config->log_file;

    FILE *flog = NULL;

    char *string = (char *)calloc(8 + strlen(msg) , sizeof(char));
    sprintf( string, "%s%s", (*msg != ':') ? "BOT$> " : "" ,msg);

    if( SHOW_LOG )
        printf( "%s", string );
    
    if( SAVE_LOG ){
        flog = fopen( log_file, "a" );
        fputs( string, flog );
        fclose( flog );
    }
}
