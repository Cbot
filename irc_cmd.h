/*  irc_cmd.h - Prototypes of the functions which allows
 *  parse and execute a set of commands into the IRC bot
 *  =====================================================
 *
 *  Copyright (C) 2009  "Carlos Ríos Vera" <crosvera@gmail.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 **********************************************************/

#include "config.h"

#define NORMAL_MSG      0
#define EXIT_RESTART    1
#define EXIT            2


/* The follow structure will be filled with the `irc_parser'
 * function, and will save some basic information about a
 * message sent to the channel.
 ***********************************************************/
struct irc_msg{
    int type;  /* Type of the message */
    char *msg;  /* The message to send*/
    char *msg_log; /* The message to send to the log */
};

struct irc_msg *irc_parser( const char *raw_entry, struct _config *config );

