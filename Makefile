#main :
#	gcc cbot.c config.c b_socket.c irc_cmd.c log.c -o cbot -g -Wall -Wextra;

#clean :
#	rm cbot
#

SRC_FILES = cbot.c config.c b_socket.c irc_cmd.c log.c
OBJ_FILES = $(patsubst %.c, %.o, ${SRC_FILES})
DEP_FILES = $(patsubst %.c, %.dep, ${SRC_FILES})

VPATH = ./

CFLAGS = -c -Wall -Wextra -g3
LDFLAGS = -g3

cbot: ${OBJ_FILES}
	gcc ${LDFLAGS} -o cbot ${OBJ_FILES}

%.o:%.c
	gcc ${CFLAGS} -o $@ $<

clean:
	rm *.o cbot

cleanall:
	rm *.o cbot *.dep

include ${DEP_FILES}

%.dep:%.c
	@set -e; rm -f $@; \
	 gcc -MM $(CFLAGS) $< > $@.$$$$; \
	 sed 's,\($*\)\.o[ :]*,\1.o $@ : ,g' < $@.$$$$ > $@; \
	 rm -f $@.$$$$



